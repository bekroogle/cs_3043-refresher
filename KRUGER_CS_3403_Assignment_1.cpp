// Benjamin Kruger
// CS_3403 FALL 2013
// August 28, 2013
// Changelog:
// September 3, 2013: Pasted prototypes, function definitions into the Modified_Driver.cpp file
// to incorporate its output options.


// Assignment_1

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

#define MAX_NUMBER_ELEMENTS 50

// Function to display menu of possible I/O streams
// Parameter specifies whether for input or output
void showMenu( string );

// Function to load array of integers from specified input stream
// First parameter is a reference to an existing input stream
// Second parameter is reference to an array of integers
// Function returns the number of elements in the array
// Preconditions: second parameter should be an empty integer array of MAX_NUMBER_ELEMENTS or fewer.
// 				  first parameter should either be the cin object or an ifstream.
// Postconditions: second parameter's elements will be replaced with integers from the first 
// 			       parameter.
// Function reads the first element extracted from the stream as the number of elements to be read 
// ( not lengthing that element )
int LoadArray( istream&, int[]);


// Function to order an array of integers from smallest to largest
// First parameter is the array of integers
// Second parameter is the number of elements in the array
// Preconditions: second parameter must be a number MAX_NUMBER_ELEMENTS or fewer.
// Postconditions: the values stored in the first parameter will be sorted in ascending order.
void sortArray( int[], int );

// Function to ouput an array of integers to an existing output stream
// First parameter is a reference to an existing output stream
// Second parameter is the array of integers
// Third parameter is the number of elements in the array
void printArray( ostream&, const int[], int );

int main()
{
	int count = 0;
	int streamType = 0;
	int arrayOfInts[ MAX_NUMBER_ELEMENTS ];
	

	showMenu( "Input" );
	cin >> streamType;

//	cout << "You chose " << streamType << endl;


	switch( streamType )
	{
	case 1:
		cout << "How many integers? " << endl;
		count = 	LoadArray( cin, arrayOfInts );
		break;
	case 2:
		string filename;
		ifstream inputFile;
		cout << "Enter the file name " << endl;
		cin >> filename;
		inputFile.open( filename );
		if ( inputFile ) 
		{
			cout << "File is " << filename << endl;
			count = LoadArray( inputFile, arrayOfInts );
			cout << "Count is " << count << endl;
			inputFile.close();

		}
		else
		{
			cout << "SOMETHING IS WRONG with the file " << endl;
			cin.get();
			system("pause");
			return 1;
		}
		

	}//switch	
	
	printArray(cout, arrayOfInts,count);

	sortArray( arrayOfInts, count );

	showMenu( "Output" );
	cin >> streamType;

	switch( streamType )
	{
	case 1:
		printArray(cout, arrayOfInts,count);
		break;
	case 2:
		string filename;
		ofstream outputFile;
		cout << "Enter the file name " << endl;
		cin >> filename;
		outputFile.open( filename );
		if ( outputFile ) 
		{
			cout << "File is " << filename << endl;
			printArray(outputFile, arrayOfInts,count);
			cout << "Count is " << count << endl;
			outputFile.close();

		}
		else
		{
			cout << "SOMETHING IS WRONG with the file " << endl;
			cin.get();
			system("pause");
			return 1;
		}
	}//case

	cin.ignore( );
	cin.ignore();
//	char wait;

	cin.get() ;
	system("pause");
	return 0;
}

void showMenu(string ioType)
{
	cout << "Choose a method for your " << ioType << ":" << endl;
	cout << "================================" << endl;
	cout << "1. Console\n2. File" << endl;
}

int LoadArray( istream& is, int arrayOfInts[])
{
    int arraySize;
	
	// Get the length of the array from the input stream
	is >> arraySize;
 
	// For each element, read a number from the input stream
	for (int i = 0; i < arraySize; i++) {
        is >> arrayOfInts[i];
    }
    return arraySize;
}

void printArray( ostream& os, const int arrayOfInts[], int length)
{
	os << "Benjamin Kruger" << endl;
	// For each element, send its value, followed by a newline to
	// the output stream
    for (int i = 0; i < length; i++) {
        os << arrayOfInts[i] << "\n";
    }
	
    os << endl;
}


void sortArray( int arrayOfInts[], int length)
{
    int i, j;
    int iMin;

    // For each element, j, in the array (except the last):
    for (j = 0; j < length - 1; j++) {

        // Assume the minimum is j.
        iMin = j;

        // For each element, i, which follows the element at index j:
        for (i = j+1; i < length; i++) {

            // If the element, i, is less than the current min value, set min to i.
            if (arrayOfInts[i] < arrayOfInts[iMin]) {
                iMin = i;
            }
        }

        // If the min value no longer equals j, swap j with min.
        if (iMin != j) {
            swap(arrayOfInts[j],arrayOfInts[iMin]);
        }
    }
}
